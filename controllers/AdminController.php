<?php

namespace app\controllers;

use app\models\Categories;
use app\models\News;
use app\models\News2Cat;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class AdminController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => 'app\behaviors\AdminBehavior',
			],
		];
	}

    public function actionNewsList()
    {
    	//SELECT news.*, categories.name FROM `news` LEFT JOIN `categories` on (SELECT cat_id from news2cats where news_id = news.id) = categories.id
		//News::find()->orderBy('create_at DESC')
		//$query->select('news.*, categories.name')->from('`news`')->leftJoin('categories', '(SELECT `cat_id` from `news2cats` where `news_id` = `news`.`id`) = `categories`.`id`')->all()
		$query = new Query();
		$dataProvider = new ActiveDataProvider([
			'query' => News::find()->orderBy('create_at DESC'),
			'pagination' => [
				'pageSize' => 10,
			],
		]);

        return $this->render('news-list', ['listDataProvider' => $dataProvider]);
    }

	public function actionAddNew()
	{
		$new = new News();

		if ($new->load(Yii::$app->request->post()))
		{
			$selectedCat = Yii::$app->request->post('cat_id');
			if ($selectedCat == null) {
				$new->cat_id = 0;
			} else {
				$new->cat_id = $selectedCat;
			}
			$new->url = Inflector::slug($new->title, '-');
			$new->save();
			$this->redirect('/admin/news-list');
		}

		return $this->render('add-new', ['model' => $new, 'categories' => Categories::getCatsTreeArray(Categories::find()->all()), 'selectedCats' => []]);
	}

	public function actionEditNew($id)
	{
		if ($id == null) {
			$this->redirect('/admin/news-list');
		}
		$new = News::findOne($id);
		$selectedCats[] = $new->category->id;
		if ($new->load(Yii::$app->request->post()))
		{
			$selectedCat = Yii::$app->request->post('cat_id');
			if ($selectedCat == null) {
				$new->cat_id = 0;
			} else {
				$new->cat_id = $selectedCat;
			}
			$new->url = Inflector::slug($new->title, '-');
			$new->save();
			$this->redirect('/admin/news-list');
		}

		return $this->render('edit-new', ['model' => $new, 'categories' => Categories::getCatsTreeArray(Categories::find()->all()), 'selectedCats' => $selectedCats]);
	}

	public function actionDeleteNew($id)
	{
		if ($id == null) {
			$this->redirect('/admin/news-list');
		}
		$new = News::findOne($id);
		if ($new->delete()) {
			$this->redirect('/admin/news-list');
		}
	}

	public function actionCatsList()
	{
		$dataProvider = new ActiveDataProvider([
			'query' => Categories::find()->orderBy('create_at DESC'),
			'pagination' => [
				'pageSize' => 10,
			],
		]);

		return $this->render('cats-list', ['listDataProvider' => $dataProvider]);
	}

	public function actionAddCat()
	{
		$cat = new Categories();
		if ($cat->load(Yii::$app->request->post()))
		{
			if ($cat->parent_id == null) {
				$cat->parent_id = 0;
			}
			if ($cat->save()) {
				$this->redirect('/admin/cats-list');
			} else {
				$this->redirect('/');
			}
		}

		return $this->render('add-cat', ['model' => $cat]);
	}

	public function actionEditCat($id)
	{
		if ($id == null) {
			$this->redirect('/admin/cats-list');
		}
		$cat = Categories::findOne($id);
		$childs = Categories::find()->select('id')->where(['parent_id' => $id])->asArray()->all();
		if ($cat->load(Yii::$app->request->post()) && $cat->save())
		{
			$this->redirect('/admin/cats-list');
		}

		return $this->render('edit-cat', ['model' => $cat, 'childs' => $childs]);
	}

	public function actionDeleteCat($id)
	{
		if ($id == null) {
			$this->redirect('/admin/cats-list');
		}
		$cat = Categories::findOne($id);
		$news = News::find()->where(['cat_id' => $id])->all();
		foreach ($news as $new) {
			$new->cat_id = 0;
			$new->save();
		}
		if ($cat->delete()) {
			$this->redirect('/admin/cats-list');
		}
	}
}
