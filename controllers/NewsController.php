<?php

namespace app\controllers;

use app\models\Categories;
use app\models\Comments;
use app\models\News;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\web\Controller;

class NewsController extends Controller
{
    public function actionShow($url)
    {
    	if ($url == null) {
    		$this->goHome();
		}
    	$new = News::find()->where(['url' => $url])->one();
    	if ($new == null) {
			$this->goHome();
		}

		$comments = Comments::find()->where(['news_id' => $new->id])->all();
    	$newComment = new Comments();
		if ($newComment->load(Yii::$app->request->post()) && $newComment->save())
		{
			$this->redirect('/news/' . $new->url);
		}

        return $this->render('show', ['new' => $new, 'comments' => $comments, 'newCom' => $newComment]);
    }

    public function actionList($catId)
	{
		$sort = new Sort([
			'attributes' => [
				'create_at' => [
					'asc' => ['create_at' => SORT_ASC],
					'desc' => ['create_at' => SORT_DESC],
					'default' => SORT_ASC,
					'label' => 'дата добавления',
				]
			],
		]);

		$dataProvider = new ActiveDataProvider([
			'query' => News::find()->where(['active' => 1, 'cat_id' => $catId])->orderBy($sort->orders),
			'pagination' => [
				'pageSize' => 3,
			],
		]);

		$category = Categories::findOne($catId);
		$cats = Categories::find()->select('categories.id, categories.name, categories.parent_id')->innerJoin('news', 'news.cat_id = categories.id')->all();

		return $this->render('list', ['listDataProvider' => $dataProvider, 'sort' => $sort, 'categories' => Categories::getCatsTreeArray($cats, 0), 'catName' => $category->name]);
	}
}
