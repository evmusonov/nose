<?php

namespace app\controllers;

use app\models\Categories;
use app\models\News;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		$sort = new Sort([
			'attributes' => [
				'create_at' => [
					'asc' => ['create_at' => SORT_ASC],
					'desc' => ['create_at' => SORT_DESC],
					'default' => SORT_DESC,
					'label' => 'дата добавления',
				]
			],
		]);

		$dataProvider = new ActiveDataProvider([
			'query' => News::find()->where(['active' => 1])->orderBy($sort->orders),
			'pagination' => [
				'pageSize' => 3,
			],
		]);

		$cats = Categories::find()->select('categories.id, categories.name, categories.parent_id')->innerJoin('news', 'news.cat_id = categories.id')->all();

        return $this->render('index', ['listDataProvider' => $dataProvider, 'sort' => $sort, 'categories' => Categories::getCatsTreeArray($cats, 0)]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
