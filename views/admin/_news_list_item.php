<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="item" data-key="<?= $model->id; ?>">
	<h4 class="title">
		#<?= $model->id ?>
        <?php if (Yii::$app->controller->id == 'admin') {
		    echo Html::a(Html::encode($model->title), Url::to(['admin/edit-new', 'id' => $model->id])) . " | " . Html::a('Удалить', Url::to(['admin/delete-new', 'id' => $model->id]));
        } else {
			echo Html::a(Html::encode($model->title), Url::to(['news/' . $model->url]));
        }
        ?>
	</h4>
    <div>Категория: <?= $model->category->name == null ? 'нет' : $model->category->name ?></div>
    <div><?= date("d.m.Y H:i:s", $model->create_at) ?></div>
	<div class="item-excerpt">
		<?= Html::encode($model->descr); ?>
	</div>
	<hr>
</div>