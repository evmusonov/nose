<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'Список новостей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
	<div class="body-content">
		<div class="row">
			<div class="col-sm-12">
				<h2><?= $this->title ?></h2>
				<div><?= Html::a('Добавить новость', Url::to(['admin/add-new']), ['class' => 'btn btn-primary']) ?></div>
				<?=
				ListView::widget([
					'dataProvider' => $listDataProvider,
					'summary' => "Показано {begin}-{end} из {totalCount} элемента(ов)",
					'layout' => "{summary}<br>{items}<br>{pager}",
					'itemView' => function ($model, $key, $index, $widget) {
						return $this->render('_news_list_item',['model' => $model]);
					},
				]);
				?>
			</div>
		</div>
	</div>
</div>