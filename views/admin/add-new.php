<?php

use app\models\Categories;
use app\models\News;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

$this->title = 'Добавить новость';
$this->params['breadcrumbs'][] = [
	'label' => 'Список новостей',
	'url' => ['/admin/news-list']
];
$this->params['breadcrumbs'][] = $this->title;

$template =
	'<div class="form-group field-advert-active">
        <div class="custom-control custom-checkbox">
            <input type="radio" id="cat_id-{id}" class="custom-control-input" name="cat_id" value="{id}" {checked}>
            <label class="custom-control-label" for="cat_id-{id}">
                <a data-toggle="collapse" href="#multiCollapse_{id}" role="button" aria-expanded="false" aria-controls="multiCollapse_{id}">{name}</a>
                {symbol}
            </label>
        </div>
    </div>';
?>

<div class="body-content">
    <div class="row">
        <div class="col-sm-12">
            <h2><?= $this->title ?></h2>
            <? $form = ActiveForm::begin([
                'id' => 'news_form',
                'validationStateOn' => 'input',
                'fieldConfig' => [
                    'template' => "<div class=\"form-group\">{label}{input}{error}</div>",
                    'labelOptions' => ['class' => ''],
                ],
                'errorCssClass' => 'is-invalid'
            ]); ?>
            <?= $form->field($model, 'title')->textInput() ?>
			<?= $form->field($model, 'descr')->textarea() ?>
			<?= $form->field($model, 'text')->textarea() ?>

            <p>Доступные категории</p>
            <div class="categories-tree">
				<?php echo Categories::buildTree($categories, 0, $template, $selectedCats); ?>
            </div>

            <?= $form->field($model, 'active')->checkbox(['checked ' => true]); ?>
            <?php ActiveForm::end(); ?>
			<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-primary btn-block', 'form' => 'news_form']) ?>
        </div>
    </div>
</div>