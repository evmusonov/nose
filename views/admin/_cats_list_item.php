<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="item" data-key="<?= $model->id; ?>">
	<h4 class="title">
		#<?= $model->id ?>
        <?= Html::a(Html::encode($model->name), Url::to(['admin/edit-cat', 'id' => $model->id])) . " | " . Html::a('Удалить', Url::to(['admin/delete-cat', 'id' => $model->id])); ?>
	</h4>
    <div><?= date("d.m.Y H:i:s", $model->create_at) ?></div>
	<hr>
</div>