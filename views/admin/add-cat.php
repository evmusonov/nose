<?php

use app\models\Categories;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

$this->title = 'Добавить категорию';
$this->params['breadcrumbs'][] = [
	'label' => 'Список категорий',
	'url' => ['/admin/cats-list']
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="body-content">
    <div class="row">
        <div class="col-sm-12">
            <h2><?= $this->title ?></h2>
            <? $form = ActiveForm::begin([
                'id' => 'cats_form',
                'validationStateOn' => 'input',
                'fieldConfig' => [
                    'template' => "<div class=\"form-group\">{label}{input}{error}</div>",
                    'labelOptions' => ['class' => ''],
                ],
                'errorCssClass' => 'is-invalid'
            ]); ?>
            <?= $form->field($model, 'name')->textInput() ?>
			<?= $form->field($model, 'parent_id')->dropdownList(
				Categories::find()->select(['name', 'id'])->indexBy('id')->column(),
				['prompt' => 'Выберите категорию']
			); ?>
            <?php ActiveForm::end(); ?>
			<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-primary btn-block', 'form' => 'cats_form']) ?>
        </div>
    </div>
</div>