<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = "$new->title - новости №1";
$this->params['breadcrumbs'][] = $new->title;
?>

<div class="row">
	<div class="col-sm-12">

		<!-- Title -->
		<h1 class="mt-4"><?= Html::encode($new->title) ?></h1>
		<!-- Date/Time -->
		<p>Опубликовано <?= date("d.m.Y H:i:s", $new->create_at) ?></p>

		<hr>

		<div><?= Html::encode($new->text) ?></div>

        <hr>

        <div class="comments">
            <h3>Комментарии</h3>
            <?php if ($comments): ?>
                <?php foreach ($comments as $com): ?>
                    <div class="comment">
                        <div class="user"><?= $com->username ?></div>
                        <div class="email"><?= $com->email ?></div>
                        <div class="clearfix"></div>
                        <div class="date"><?= date("d.m.Y H:i:s", $com->create_at) ?></div>
                        <div class="text"><?= Html::encode($com->text) ?></div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div>Нет комментариев</div>
			<?php endif; ?>
        </div>
        <hr>
        <div class="add-comment">
            <h3>Добавить комментарий</h3>
			<? $form = ActiveForm::begin([
				'id' => 'comments_form',
				'validationStateOn' => 'input',
				'fieldConfig' => [
					'template' => "<div class=\"form-group\">{label}{input}{error}</div>",
					'labelOptions' => ['class' => ''],
				],
				'errorCssClass' => 'is-invalid'
			]); ?>
			<?= $form->field($newCom, 'username')->textInput() ?>
			<?= $form->field($newCom, 'email')->textInput() ?>
			<?= $form->field($newCom, 'text')->textarea() ?>
			<?= $form->field($newCom, 'news_id')->hiddenInput(['value' => $new->id])->label(false) ?>
			<?php ActiveForm::end(); ?>
			<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' => 'btn btn-primary btn-block', 'form' => 'comments_form']) ?>
        </div>
	</div>
</div>
