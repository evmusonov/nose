<?php

/* @var $this yii\web\View */

use app\models\Categories;
use yii\widgets\ListView;

$this->title = 'Nose.ru';

$template =
	'<div class="form-group field-advert-active">
        <div class="custom-control">
            <a href="/news/list/{id}">{name}</a>
            {symbol}
        </div>
    </div>';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-sm-12">
                <h2>Новости</h2>
                <div class="dropdown mydropdown">
                    <a class="btn btn-primary" href="#" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Меню категорий</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown07">
                        <div class="categories-tree">
							<?php echo Categories::buildTree($categories, 0, $template, [], 1); ?>
                        </div>
                    </div>
                </div>
                Сортировка по <?= $sort->link('create_at') ?>
                <?=
                    ListView::widget([
						'dataProvider' => $listDataProvider,
						'summary' => "Показано {begin}-{end} из {totalCount} элемента(ов)",
						'layout' => "{summary}<br>{items}<br>{pager}",
						'itemView' => function ($model, $key, $index, $widget) {
							return $this->render('/admin/_news_list_item',['model' => $model]);
						},
                    ]);
                ?>
            </div>
        </div>
    </div>
</div>
