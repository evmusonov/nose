<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class News2Cat extends \yii\db\ActiveRecord
{
	/**
	 * @return string
	 */
	public static function tableName()
	{
		return '{{news2cats}}';
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'create_at',
					ActiveRecord::EVENT_BEFORE_UPDATE => 'modify_at',
				],
				'value' => function () {
					return date('U');
				},
			],
		];
	}

	public function rules()
	{
		return [
			['news_id', 'integer'],
			['cat_id', 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'news_id' => 'Новость',
			'cat_id'  => 'Категория',
		];
	}
}