<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class News extends \yii\db\ActiveRecord
{
	/**
	 * @return string
	 */
	public static function tableName()
	{
		return '{{news}}';
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'create_at',
					ActiveRecord::EVENT_BEFORE_UPDATE => 'modify_at',
				],
				'value' => function () {
					return date('U');
				},
			],
		];
	}

	public function rules()
	{
		return [
			['title', 'required', 'message' => 'Заполните заголовок'],
			['descr', 'required', 'message' => 'Заполните описание'],
			['text', 'required', 'message' => 'Заполните текст'],
			['cat_id', 'integer'],
			['url', 'string'],
			['active', 'safe']
		];
	}

	public function attributeLabels()
	{
		return [
			'title'  => 'Заголовок',
			'descr'  => 'Описание',
			'text'   => 'Текст',
			'cat_id' => 'Категория',
			'active' => 'Активно'
		];
	}

	public function getCategory()
	{
		return $this->hasOne(Categories::className(), ['id' => 'cat_id']);
	}
}