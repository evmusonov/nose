<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Categories extends \yii\db\ActiveRecord
{
	/**
	 * @return string
	 */
	public static function tableName()
	{
		return '{{categories}}';
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'create_at',
					ActiveRecord::EVENT_BEFORE_UPDATE => 'modify_at',
				],
				'value' => function () {
					return date('U');
				},
			],
		];
	}

	public function rules()
	{
		return [
			['name', 'required', 'message' => 'Заполните название'],
			['parent_id', 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'name'      => 'Название',
			'parent_id' => 'Родительская категория',
		];
	}

	/**
	 * Returns array of categories ids for output tree
	 *
	 * @param object $query
	 * @return array
	 */
	public static function getCatsTreeArray($query, $prompt = 1)
	{
		if ($prompt) {
			$categories = [0 => [-1 => 'Без категории']];
		} else {
			$categories = [];
		}
		foreach ($query as $cat) {
			$categories[$cat->parent_id][$cat->id] = $cat->name;
		}

		return $categories;
	}

	/**
	 * Build output tree of categories
	 *
	 * @param array $cats
	 * @param int $parent_id
	 * @param string $template
	 * @param array $cheked_arr
	 * @param int $show
	 * @return null|string
	 */
	public static function buildTree($cats, $parent_id, $template, $cheked_arr, $show = 0)
	{
		if (is_array($cats) && isset($cats[$parent_id])) {
			$class = $parent_id == 0 ? '' : 'class="subtree"';
			$class .= ($parent_id != 0 && $show == 0) ? ' style="display:none;"' : '';
			$tree = '<ul '.$class.' >';
			foreach($cats[$parent_id] as $id => $cat) {
				$firstLi = is_array($cats[$id]) ? 'class="lisubtree"' : '';
				$tree .= '<li '.$firstLi.'>';
				$tree .= self::replaceTemplate($template, $cat, $id, $cheked_arr, $cats);
				unset($cats[$parent_id][$id]);
				$tree .=  self::buildTree($cats, $id, $template, $cheked_arr, $show);
				$tree .= '</li>';
			}
			$tree .= '</ul>';
		} else {
			return null;
		}

		return $tree;
	}

	/**
	 * @param string $template
	 * @param string $name
	 * @param int $id
	 * @param array $cheked_arr
	 * @param array $cats
	 * @return string
	 */
	public static function replaceTemplate($template, $name, $id, $cheked_arr = [], $cats)
	{
		$temp = $template;
		$temp = str_replace('{name}', $name, $temp);
		$temp = str_replace('{id}', $id, $temp);
		$temp = str_replace('{symbol}', is_array($cats[$id]) ? '&darr;' : '', $temp);
		$temp = str_replace('{checked}',
			(array_search($id, $cheked_arr) === FALSE) ? '' : 'checked', $temp);

		return $temp;
	}
}