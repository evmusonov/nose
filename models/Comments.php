<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Comments extends \yii\db\ActiveRecord
{
	/**
	 * @return string
	 */
	public static function tableName()
	{
		return '{{comments}}';
	}

	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => 'create_at',
					ActiveRecord::EVENT_BEFORE_UPDATE => 'modify_at',
				],
				'value' => function () {
					return date('U');
				},
			],
		];
	}

	public function rules()
	{
		return [
			['username', 'required', 'message' => 'Введите имя'],
			['email', 'required', 'message' => 'Введите почту'],
			['text', 'required', 'message' => 'Заполните текст'],
			['news_id', 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'username' => 'Имя',
			'email'    => 'Почта',
			'text'     => 'Текст',
		];
	}
}