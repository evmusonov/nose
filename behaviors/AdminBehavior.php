<?php

namespace app\behaviors;

use yii\base\Behavior;
use yii\console\Controller;

class AdminBehavior extends Behavior
{
	public function events()
	{
		return [
			Controller::EVENT_BEFORE_ACTION => 'beforeAction'
		];
	}

	public function beforeAction()
	{
		if (!(\Yii::$app->getUser()->identity->username == 'admin'))
		{
			\Yii::$app->getResponse()->redirect(\Yii::$app->homeUrl)->send();
		}
	}
}